package org.coursera.algorithms.week3

/**
 * @author roman.gorodyshcher
 */
object KargerMinCutRunner {
  def main(args: Array[String]) = {
    val (vertexes, edges) = scala.io.Source.fromFile(args(0)).getLines().foldLeft((Set[Int](), List[(Int, Int)]())) {
      case ((vs, es), line) =>
        val row = line.split(" |\t").filterNot(_ == "").map(_.toInt)
        val biggerNodes = row.tail.filter(_ >= row.head)
        (vs + row.head, List.fill(biggerNodes.size)(row.head).zip(biggerNodes) ++ es)
    }
    println(KargerMinCutFinder.findMinCutSize(vertexes, edges))
  }
}

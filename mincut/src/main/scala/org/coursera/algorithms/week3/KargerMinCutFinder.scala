package org.coursera.algorithms.week3

import scala.util.Random
import scala.annotation.tailrec

/**
 * @author roman.gorodyshcher
 */
object KargerMinCutFinder {

  private val random = new Random(System.currentTimeMillis)

  def findMinCutSize(vertexes: Set[Int], edges: List[(Int, Int)]): Int = {
    (1 to edges.size).foldLeft(edges.size)((minCut, i) => {
      val cutSize = kargerMinCutIteration(vertexes, edges)
      if (cutSize < minCut) { println(s"$i -> $cutSize"); cutSize } else minCut
    })
  }

  @tailrec
  private def kargerMinCutIteration(vertexes: Set[Int], edges: List[(Int, Int)]): Int = {
    val edgesExceptSelfLoops = edges.filter(e => e._1 != e._2)
    val randEdge = edgesExceptSelfLoops(random.nextInt(edgesExceptSelfLoops.size))

    if (vertexes.size <= 2) edgesExceptSelfLoops.size
    else {
      val updatedEdges = edgesExceptSelfLoops map {
        case (l, r) if l == randEdge._2 => (randEdge._1, r)
        case (l, r) if r == randEdge._2 => (l, randEdge._1)
        case notAffected => notAffected
      }
      kargerMinCutIteration(vertexes filter (_ != randEdge._2), updatedEdges)
    }
  }
}

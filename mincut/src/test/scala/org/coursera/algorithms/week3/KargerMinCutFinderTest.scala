package org.coursera.algorithms.week3

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class KargerMinCutFinderTest extends FlatSpec {
  "Mincut of a graph of two nodes without selfloops" should "be the same as total edges count" in {
    assert(KargerMinCutFinder.findMinCutSize(Set(1, 2), List((1, 2), (2, 1))) == 2)
  }

  "Mincut of a graph of two nodes with selfloops" should "be the same as total number of edges not counting selfloop edges" in {
    assert(KargerMinCutFinder.findMinCutSize(Set(1, 2), List((1, 2), (2, 1), (1, 1), (2, 2), (2, 1))) == 3)
  }

  """
    |1--2---3--4
    ||\/|   |\/|
    ||/\|   |/\|
    |5--6---7--8
  """.stripMargin should "have minimum cut of two edges (2,3) and (6, 7)" in {
    val vertexes = (1 to 8).toSet
    val edges = List(
      (1, 2), (2, 3), (3, 4),
      (1, 5), (1, 6), (2, 5), (2, 6), (3, 7), (3, 8), (4, 7), (4, 8),
      (5, 6), (6, 7), (7, 8)
    )
    val minCut = KargerMinCutFinder.findMinCutSize(vertexes, edges)
    assert(minCut == 2)
  }
}

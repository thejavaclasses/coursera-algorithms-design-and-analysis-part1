package org.coursera.algorithms.week6

import scala.collection.immutable.TreeSet
import scala.annotation.tailrec

/**
 * @author roman.gorodyshcher
 */
object MediansSumCounter {

  def countMediansSum(numbers: List[Int]): Long = countMediansSum(numbers, TreeSet[Int](), TreeSet[Int](), 0L)

  @tailrec
  private def countMediansSum(numbers: List[Int], smallest: TreeSet[Int], biggest: TreeSet[Int], sum: Long): Long = numbers match {
    case Nil => sum
    case h::tail =>
      if (smallest.isEmpty && biggest.isEmpty)
        countMediansSum(tail, smallest + h, biggest, h.toLong)
      else if ((h <= smallest.max) && smallest.size == biggest.size)
        countMediansSum(tail, smallest + h, biggest, sum + smallest.max)
      else if (h <= smallest.max && smallest.size > biggest.size) {
        val transfer = smallest.max
        val updatedSmallest = smallest - transfer + h
        countMediansSum(tail, updatedSmallest, biggest + transfer, sum + updatedSmallest.max)
      }
      else if (biggest.isEmpty || h >= biggest.min && biggest.size < smallest.size)
        countMediansSum(tail, smallest, biggest + h, sum + smallest.max)
      else if (h >= biggest.min && biggest.size == smallest.size) {
        val transfer = biggest.min
        countMediansSum(tail, smallest + transfer, biggest - transfer + h, sum + transfer)
      }
      else {
        if (smallest.size == biggest.size)
          countMediansSum(tail, smallest + h, biggest, sum + h)
        else
          countMediansSum(tail, smallest, biggest + h, sum + smallest.max)
      }
  }

}

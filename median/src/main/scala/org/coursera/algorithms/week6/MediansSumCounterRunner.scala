package org.coursera.algorithms.week6

/**
 * @author roman.gorodyshcher
 */
object MediansSumCounterRunner {

  def main(args: Array[String]) = {
    val numbers = scala.io.Source.fromFile(args(0)).getLines().map(_.toInt).toList
    println(MediansSumCounter.countMediansSum(numbers))
  }
}

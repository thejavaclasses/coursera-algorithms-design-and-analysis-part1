package org.coursera.algorithms.week6

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class MediansSumCounterTest extends FlatSpec {

  "[5, 3, 10, 8, 9, 2, 2]" should "have medians sum 36" in {
    val result = MediansSumCounter.countMediansSum(List(5, 3, 10, 8, 9, 2, 2))
    assert(result == 36)  // 5 + 3 + 5 + 5 + 8 + 5 + 5
  }

}

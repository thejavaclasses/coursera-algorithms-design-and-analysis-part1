package org.coursera.algorithms.week5

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class DijkstraShortestPathsFinderTest extends FlatSpec {

  "DijkstraShortestPathsFinder" should "find shortest paths" in {
    val graph = Map(
      1 -> List((2, 7L), (6, 14L), (3, 9L)),
      2 -> List((1, 7L), (3, 10L), (4, 15L)),
      3 -> List((1, 9L), (2, 10L), (6, 2L), (4, 11L)),
      4 -> List((2, 15L), (3, 11L), (5, 6L)),
      5 -> List((4, 6L), (6, 9L)),
      6 -> List((1, 14L), (3, 2L), (5, 9L))
    )
    val shortestPaths = DijkstraShortestPathsFinder.findShortestPaths(graph, 1).toMap
    assert(shortestPaths.size == 6)
    assert(shortestPaths(1) == 0)
    assert(shortestPaths(2) == 7)
    assert(shortestPaths(3) == 9)
    assert(shortestPaths(4) == 20)
    assert(shortestPaths(5) == 20)
    assert(shortestPaths(6) == 11)
  }

}

package org.coursera.algorithms.week5

/**
 * @author roman.gorodyshcher
 */
object DijkstraShortestPathsFinderRunner {

  def main(args: Array[String]) = {
    val graph = scala.io.Source.fromFile(args(0)).getLines().foldLeft(Map[Int, List[(Int, Long)]]()) {
      case (g, l) =>
        val row = l.split("\t|,| ").map(_.toInt)
        val vertex = row.head
        val edges = row.tail.grouped(2).map(p => (p(0), p(1).asInstanceOf[Long])).toList
        g + (vertex -> edges)
    }
    val paths = DijkstraShortestPathsFinder.findShortestPaths(graph, 1).toMap.withDefaultValue(1000000L)
    val answer = List(paths(7), paths(37), paths(59), paths(82), paths(99), paths(115), paths(133), paths(165), paths(188), paths(197))
    println(answer.mkString(","))
  }
}

package org.coursera.algorithms.week5

import scala.annotation.tailrec

/**
 * @author roman.gorodyshcher
 */
object DijkstraShortestPathsFinder {

  type Graph = Map[Int, List[(Int, Long)]]
  type Distances = List[(Int, Long)]

  def findShortestPaths(graph: Graph, start: Int): Distances = findShortestPaths(graph, List((start, 0L)), Nil)

  @tailrec
  private def findShortestPaths(graph: Graph, queue: Distances, found: Distances): Distances = {
    if (queue.isEmpty) found
    else {
      val f @ (next, dist) = queue.minBy { case (v, d) => d }
      val neighboursMap = graph(next).filterNot { case (v, _) => found.exists { case (fv, _) => v == fv } }.toMap

      val actualQueue = queue.filterNot { case (v, d) => v == next || (neighboursMap.contains(v) && neighboursMap(v) + dist <= d) }
      findShortestPaths(graph, actualQueue ++ neighboursMap.toList.map { case (v, d) => (v, d + dist) }, f::found)
    }
  }
}

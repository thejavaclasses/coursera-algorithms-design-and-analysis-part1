package org.coursera.algorithms.week2

import scala.annotation.tailrec

/**
 * @author roman.gorodyshcher
 */
object QuickSortCounter {
  def sortAndCountComparisons(pivotPicker: PivotPicker)(unsorted: List[Int]): (List[Int], Int) = unsorted match {
    case Nil => (Nil, 0)
    case single @ h::Nil => (single, 0)
    case many =>
      val (smaller, pivots, greater) = pivotPicker.partition(many, pivotPicker.pivot(many))
      val smallerSorted = sortAndCountComparisons(pivotPicker)(smaller)
      val greaterSorted = sortAndCountComparisons(pivotPicker)(greater)
      val comparisons = many.size - 1
      (smallerSorted._1 ++ pivots ++ greaterSorted._1, smallerSorted._2 + greaterSorted._2 + comparisons)
  }
}

sealed trait PivotPicker {
  def pivot(range: List[Int]): Int
  def partition(range: List[Int], pivot: Int): (List[Int], List[Int], List[Int])
}

case object PickFirstAsPivot extends PivotPicker {
  override def pivot(range: List[Int]) = range.head
  override def partition(range: List[Int], pivot: Int): (List[Int], List[Int], List[Int]) = partition(range, pivot, Nil, Nil, Nil)

  @tailrec
  private def partition(range: List[Int], pivot: Int, smaller: List[Int], pivots: List[Int], greater: List[Int]): (List[Int], List[Int], List[Int]) = {
    (range, greater) match {
      //simulating the last elements from smaller part being swapped with pivots
      case (Nil, _) => (smaller.take(pivots.size) ++ smaller.drop(pivots.size).reverse, pivots, greater.reverse)
      case (h::tail, Nil) if h < pivot => partition(tail, pivot, h::smaller, pivots, greater)
      //simulating the first element from greater part being swapped with an element to the right which is smaller than pivot
      //so we have pivot(s), then uninterrupted smaller elements part, then uninterrupted greater elements part, then not partitioned part
      case (h::tail, gs) if h < pivot => partition(tail, pivot, h::smaller, pivots, gs.takeRight(1) ++ gs.dropRight(1))
      case (h::tail, _) if h > pivot => partition(tail, pivot, smaller, pivots, h::greater)
      case (h::tail, _) => partition(tail, pivot, smaller, h::pivots, greater)
    }
  }
}

case object PickLastAsPivot extends PivotPicker {
  override def pivot(range: List[Int]) = range.last
  override def partition(range: List[Int], pivot: Int): (List[Int], List[Int], List[Int]) =
    PickFirstAsPivot.partition(range.tail.takeRight(1) ++ range.tail.dropRight(1) ++ List(range.head), pivot)
}

case object PickMedianOfThreeAsPivot extends PivotPicker {
  override def pivot(range: List[Int]): Int = {
    val l = range.head
    val r = range.last
    val m = range.take((range.size + 1) / 2).last
    val min = List(l, r, m).min
    val max = List(l, r, m).max
    if (l != min && l != max) l
    else if (r != min && r != max) r
    else m
  }

  override def partition(range: List[Int], pivot: Int): (List[Int], List[Int], List[Int]) =
    if (pivot == range.head) PickFirstAsPivot.partition(range, pivot)
    else if (pivot == range.last) PickLastAsPivot.partition(range, pivot)
    else {
      partition(
        List(pivot)
          ++ range.take((range.size + 1) / 2).tail.dropRight(1)
          ++ List(range.head)
          ++ range.drop((range.size + 1) / 2), pivot)
    }
}
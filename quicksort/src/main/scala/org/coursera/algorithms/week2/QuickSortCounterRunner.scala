package org.coursera.algorithms.week2

/**
 * @author roman.gorodyshcher
 */
object QuickSortCounterRunner {
  def main(args: Array[String]) = {
    val list = scala.io.Source.fromFile(args(0)).getLines().map(_.toInt).toList
    println("PickFirstAsPivot comparisons: " + QuickSortCounter.sortAndCountComparisons(PickFirstAsPivot)(list)._2)
    println("PickLastAsPivot comparisons: " + QuickSortCounter.sortAndCountComparisons(PickLastAsPivot)(list)._2)
    println("PickMedianOfThreeAsPivot comparisons: " + QuickSortCounter.sortAndCountComparisons(PickMedianOfThreeAsPivot)(list)._2)
  }
}

package org.coursera.algorithms.week2

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class QuickSortCounterTest extends FlatSpec {
  "[1, 3, 5, 2, 4, 6]" should "be sorted after QuickSort" in new PickFirstAsPivotConfiguration {
    val sorted = sortAndCount(List(1, 3, 5, 2, 4, 6))._1
    assert(sorted == List(1, 2, 3, 4, 5, 6))
  }

  "[7, 5, 1, 4, 8, 3, 10, 2, 6, 9], p = 7" should "be sorted and have 24 comparisons" in new PickFirstAsPivotConfiguration {
    val result = sortAndCount(List(7, 5, 1, 4, 8, 3, 10, 2, 6, 9))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 24)
  }

  "[8, 10, 1, 9, 7, 2, 6, 3, 5, 4], p = 8" should "be sorted and have 21 comparisons" in new PickFirstAsPivotConfiguration {
    val result = sortAndCount(List(8, 10, 1, 9, 7, 2, 6, 3, 5, 4))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 21)
  }

  "[7, 5, 1, 4, 8, 3, 10, 2, 6, 9], p = 9" should "be sorted and have 23 comparisons" in new PickLastAsPivotConfiguration {
    val result = sortAndCount(List(7, 5, 1, 4, 8, 3, 10, 2, 6, 9))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 23)
  }

  "[8, 10, 1, 9, 7, 2, 6, 3, 5, 4], p = 4" should "be sorted and have 20 comparisons" in new PickLastAsPivotConfiguration {
    val result = sortAndCount(List(8, 10, 1, 9, 7, 2, 6, 3, 5, 4))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 20)
  }

  "[7, 5, 1, 4, 8, 3, 10, 2, 6, 9], p = 8" should "be sorted and have 20 comparisons" in new PickMedianOfThreeAsPivotConfiguration {
    val result = sortAndCount(List(7, 5, 1, 4, 8, 3, 10, 2, 6, 9))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 20)
  }

  "[8, 10, 1, 9, 7, 2, 6, 3, 5, 4], p = 7" should "be sorted and have 19 comparisons" in new PickMedianOfThreeAsPivotConfiguration {
    val result = sortAndCount(List(8, 10, 1, 9, 7, 2, 6, 3, 5, 4))
    assert(result._1 == List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert(result._2 == 19)
  }

  trait PickFirstAsPivotConfiguration {
    val sortAndCount = QuickSortCounter.sortAndCountComparisons(PickFirstAsPivot)_
  }

  trait PickLastAsPivotConfiguration {
    val sortAndCount = QuickSortCounter.sortAndCountComparisons(PickLastAsPivot)_
  }

  trait PickMedianOfThreeAsPivotConfiguration {
    val sortAndCount = QuickSortCounter.sortAndCountComparisons(PickMedianOfThreeAsPivot)_
  }
}
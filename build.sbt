name := "Stanford Algorithms Course, Part 1"

version := "1.0"

scalaVersion := "2.10.3"

lazy val inversions = project

lazy val quicksort = project

lazy val mincut = project

lazy val sccs = project

lazy val dijkstra = project

lazy val twosum = project

lazy val median = project

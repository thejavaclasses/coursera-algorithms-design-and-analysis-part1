package org.coursera.algorithms.week1

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class InversionsCounterTest extends FlatSpec {
  "[1, 3, 5, 2, 4, 6]" should "have 3 inversions" in {
    assert(InversionsCounter.countInversions(List(1, 3, 5, 2, 4, 6)) == 3)
  }
}

package org.coursera.algorithms.week1

/**
 * @author roman.gorodyshcher
 */
object InversionsCounter {

  def countInversions(list: List[Int]): Long =
    if (list.size < 2) 0
    else mergeSortAndCountInversions(list)._2

  private def mergeSortAndCountInversions(list: List[Int]): (List[Int], Long) = list match {
    case singleton @ h::Nil => (singleton, 0)
    case _ =>
      val parts = list.splitAt(list.size / 2)
      val left = mergeSortAndCountInversions(parts._1)
      val right = mergeSortAndCountInversions(parts._2)
      val mergeResult = mergeAndCountInversions(left._1, right._1, Nil, 0L)
      (mergeResult._1, mergeResult._2 + left._2 + right._2)
  }

  private def mergeAndCountInversions(left: List[Int], right: List[Int], invResult: List[Int], invC: Long): (List[Int], Long) = (left, right) match {
    case (l::ls, r::rs) =>
      if (l <= r) {
        mergeAndCountInversions(ls, right, l::invResult, invC)
      } else {
        mergeAndCountInversions(left, rs, r::invResult, invC + left.size)
      }
    case (Nil, Nil) => (invResult.reverse, invC)
    case (ls, Nil) => (invResult.reverse ++ ls, invC)
    case (Nil, rs) => (invResult.reverse ++ rs, invC)
  }
}

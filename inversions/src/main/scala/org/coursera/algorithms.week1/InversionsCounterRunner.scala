package org.coursera.algorithms.week1

/**
 * @author roman.gorodyshcher
 */
object InversionsCounterRunner {
  def main(args: Array[String]) = {
    val list = scala.io.Source.fromFile(args(0)).getLines().map(_.toInt).toList
    print(InversionsCounter.countInversions(list))
  }
}

package org.coursera.algorithms.week6

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class TwoSumCounterTest extends FlatSpec {

  "[-100, 50, 50, 150]" should "have 1 2-sum in range (0-99)" in {
    val result = TwoSumCounter.countAllTwoSums(List(-100L, 50L, 50L, 150L), Range.inclusive(0, 99))
    assert(result == 1)
  }

  it should "have 1 2-sums in range (0-100)" in {
    val result = TwoSumCounter.countAllTwoSums(List(-100L, 50L, 50L, 150L), Range.inclusive(0, 100))
    assert(result == 1)
  }

  "[-100, 50, 51, 150]" should "have 1 2-sum in range (0-100)" in {
    val result = TwoSumCounter.countAllTwoSums(List(-100L, 50L, 51L, 150L), Range.inclusive(0, 100))
    assert(result == 1)
  }

  "[-100, 50, 51, 150]" should "have 2 2-sum in range (0-101)" in {
    val result = TwoSumCounter.countAllTwoSums(List(-100L, 50L, 51L, 150L), Range.inclusive(0, 101))
    assert(result == 2)
  }

}

package org.coursera.algorithms.week6

import scala.collection.JavaConversions._
import java.util

/**
 * @author roman.gorodyshcher
 */
object TwoSumCounter {

  def countAllTwoSums(src: List[Long], range: Range): Int = {
    //using java hashset gives 3x performance boost here
    val numbersSet = new util.HashSet[Long](src)
    val foundSums = new util.HashSet[Int]()

    range.par.foreach(target => {
      if (target % 10 == 0) println(s"Computing 2sum for [$target]")
      src.foreach(first => {
        val second = target.toLong - first
        if (second > first && numbersSet.contains(second))
          foundSums.add(target)
      })
    })

    foundSums.size()
  }
}

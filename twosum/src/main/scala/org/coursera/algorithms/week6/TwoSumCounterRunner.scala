package org.coursera.algorithms.week6

/**
 * @author roman.gorodyshcher
 */
object TwoSumCounterRunner {
  def main(args: Array[String]) = {
    val list = scala.io.Source.fromFile(args(0)).getLines().map(_.toLong).toList
    val result = TwoSumCounter.countAllTwoSums(list, Range.inclusive(-10000, 10000))
    println(result)
  }
}
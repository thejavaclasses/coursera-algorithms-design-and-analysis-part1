package org.coursera.algorithms.week4

import org.scalatest.FlatSpec

/**
 * @author roman.gorodyshcher
 */
class SCCFinderTest extends FlatSpec {

  """
    |1 --o 2
    | o    o
    |  \   |
    |   \  |
    |    \ |
    |      3
  """.stripMargin should "have 3 SCCs" in {
    val sccs = SCCFinder.computeStronglyConnectedComponents(List(
      (1, 2),
      (3, 1),
      (3, 2)
    ))
    assert(sccs.size == 3)
  }

  """
    |1 ----o 7 ----o 9 ----o 6 ----o 8 o-- 5
    | o      |       o       |       |    o
    |  \     |        \     /        |   /
    |   \   /          \   /         |  /
    |    \ o            \ o          o /
    |     4              3           2
    |
  """.stripMargin should "have 3 SCCs of 3 nodes each" in {
    val sccs = SCCFinder.computeStronglyConnectedComponents(List(
      (1, 7),
      (7, 9),
      (4, 1),
      (7, 4),
      (3, 9),
      (6, 3),
      (9, 6),
      (6, 8),
      (8, 2),
      (2, 5),
      (5, 8)
    ))
    assert(sccs.size == 3)
    sccs foreach (c => assert(c.size == 3))
  }

}

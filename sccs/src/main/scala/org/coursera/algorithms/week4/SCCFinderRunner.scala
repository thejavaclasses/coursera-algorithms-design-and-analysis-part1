package org.coursera.algorithms.week4

/**
 * @author roman.gorodyshcher
 */
object SCCFinderRunner {
  def main(args: Array[String]) = {
    val edges = scala.io.Source.fromFile(args(0)).getLines().map(line => {
      val row = line.split(" |\t").filterNot(_ == "").map(_.toInt)
      assert(row.size == 2)
      (row(0), row(1))
    })
    val sccsSizes = SCCFinder.computeStronglyConnectedComponents(edges.toList) map (_.size)
    println(sccsSizes.sorted.takeRight(5).mkString(","))
  }
}
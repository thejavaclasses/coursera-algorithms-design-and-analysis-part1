package org.coursera.algorithms.week4

import scala.annotation.tailrec
import scala.collection.JavaConversions._


/**
 * @author roman.gorodyshcher
 */
object SCCFinder {

  def computeStronglyConnectedComponents(edges: List[(Int, Int)]): List[Set[Int]] = {
    println("Started building graph..")
    val graph = buildGraph(edges)
    println(s"Built graph of ${graph.keySet.size} nodes")
    val visited = new java.util.HashSet[Int]()
    val nextTraverseOrder = graph.keys.foldLeft(List[Int]())((path, current) => {
      println(s"Visited ${visited.size} nodes.")
      val foundPath = bfs(graph, visited, current)
      visited addAll foundPath
      foundPath ++ path
    })
    println("Computed traverse order")

    val transposedGraph = buildGraph(edges map { case (from, to) => (to, from) })
    val transposedVisited = new java.util.HashSet[Int]()

    println("Built transposed graph")
    nextTraverseOrder.foldLeft(List[Set[Int]]())((sccs, current) => {
      val sccNodes = bfs(transposedGraph, transposedVisited, current)
      transposedVisited addAll sccNodes
      sccNodes.toSet :: sccs
    }).filter(_.size > 0)
  }

  private def bfs(graph: Map[Int, List[Int]], alreadyVisited: java.util.Set[Int], start: Int): List[Int] = {

    @tailrec
    def bfs(queue: java.util.LinkedList[Int], visited: java.util.Set[Int], traversal: List[Int]): List[Int] = {
      if (queue.isEmpty) traversal
      else if (visited contains queue.get(0)) {
        queue.remove(0)
        bfs(queue, visited, traversal)
      } else {
        if (visited.size % 1000 == 0) println(s"Visited ${visited.size}, in queue ${queue.size}")
        val head = queue.remove(0)
        visited.add(head)
        val next = graph(head).filterNot(n => visited.contains(n))
        queue addAll next

        bfs(queue, visited, head :: traversal)
      }
    }

    val startQueue = new java.util.LinkedList[Int]()
    startQueue.add(start)
    bfs(startQueue, alreadyVisited, Nil).reverse
  }

  private def buildGraph(edges: List[(Int, Int)]): Map[Int, List[Int]] =
    edges.foldLeft(Map[Int, List[Int]]().withDefaultValue(Nil))((graph, edge) => {
      val withOneNode = graph + (edge._1 -> (edge._2::graph(edge._1)))
      if (!(graph contains edge._2)) withOneNode + (edge._2 -> Nil) else withOneNode
    })
}
